﻿// С++18.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include<string>

using namespace std;

class player
{
private:
	string namePlayer;
	int scorePlayer;
public:
	player() : namePlayer("Someone"), scorePlayer(0)
	{}

	player(string _namePlayer, int _scorePlayer) : namePlayer(_namePlayer), scorePlayer(_scorePlayer)
	{}

	void Show()
	{
		cout << endl << namePlayer << " : " << scorePlayer;
	}
	
	int getScorePlayer()
	{
		return scorePlayer;
	}

	void setScorePlayer(int newScorePlayer)
	{
		scorePlayer = newScorePlayer;
	}

	void setPlayer(string newNamePlayer, int newScorePlayer)
	{
		namePlayer = newNamePlayer;
		scorePlayer = newScorePlayer;
	}
};


int main()
{
    setlocale(LC_ALL, "Rus");

	cout << "Сколько игроков вы хотете добавить ? " << endl;
	int SIZE;
	cin >> SIZE;

	string arrName;
	int arrScore;

	player* base = new player[SIZE];
    
	for (int i = 0, j = 0; i < SIZE; i++, j++)
	{
		string a;
		int b;

		cout << " Введите " << i + 1 << " имя: ";
		cin >> a;
		arrName = a;

		cout << "Введите счёт: " << a << ' ';
		cin >> b;
		arrScore = b;

		base[i].setPlayer(arrName, arrScore);

	}

	cout << " Вы ввели: " << endl;

	for (int i = 0; i < SIZE; i++)
	{
		base[i].Show();
			
	}


	for (int j = 1; j < SIZE; j++)
	{
		for (int r = SIZE - 1; r >= j; r--)
		{
			if (base[r - 1].getScorePlayer() < base[r].getScorePlayer())
			{
				player temp = base[r - 1];
				base[r - 1] = base[r];
				base[r] = temp;
			}
		}
	}

	cout << endl << endl << " Отсортированный массив : ";
	for (int i = 0; i < SIZE; i++)
	{
		base[i].Show();
	}

	return 0;
}


